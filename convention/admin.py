# +-+ coding: utf-8 +-+

from django.contrib import admin
from convention.models import Booking, Transaction, Package, Sleep, Event, Seite, TShirt, Tag, Dokument, Anhang, Tarif, Disziplin
from django.db.models import Sum, Count, F, When, Case, IntegerField
from django.db.models.functions import Coalesce
from django.utils.html import format_html
from django.utils import timezone
from import_export import resources
from import_export.admin import ExportMixin
from import_export.fields import Field
from django.template import defaultfilters

# Register your models here.

class SleepAdmin(admin.ModelAdmin):
    list_display = ("code", "name", "show_pakete", "preis", "anzahl", "show_frei", "order")
    list_display_links = ("code",)
    search_fields = ("code", "name" )

    def get_queryset(self, request):
        qs = Sleep.objects.prefetch_related("packages", "booking_set")
        if not request.user.is_superuser:
            qs = qs.filter(packages__event__admin=request.user).distinct()
        qs = qs.annotate(
            frei=F("anzahl")-Coalesce(Count(
                "booking"
            ), 0)
        )

        return qs

    def show_frei(self, inst):
        return inst.frei 
    show_frei.short_description = "Frei"

    def show_pakete(self, inst):
        return "; ".join(inst.packages.all().values_list("code", flat=True))
    show_pakete.short_description = "Pakete"



class PackageAdmin(admin.ModelAdmin):
    list_display = ("event", "code", "name", "kurz", "preis", "naechte", "buchbar", "order")
    list_display_links = ("code",)
    list_filter = ("naechte",)
    search_fields = ("code", "name", "kurz")

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if not request.user.is_superuser:
            if db_field.name == "event":
                kwargs["queryset"] = Event.objects.filter(admin=request.user)

        return super(PackageAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)


    def get_queryset(self, request):
        qs = super(PackageAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(event__admin=request.user)

class TransactionInline(admin.TabularInline):
    model = Transaction

class TShirtInline(admin.TabularInline):
    model = TShirt

class TagInline(admin.TabularInline):
    model = Tag

class DisziplinInline(admin.TabularInline):
    model = Disziplin

class TarifInline(admin.TabularInline):
    model = Tarif

class DokumentInline(admin.TabularInline):
    model = Dokument

class AnhangInline(admin.TabularInline):
    model = Anhang

def checkin(modeladmin, request, queryset):
    for b in queryset:
        b.checkin = timezone.now()
        b.save()

checkin.short_description = "Einchecken" 

class BookingResource(resources.ModelResource):
    class Meta:
        model = Booking
    
    def dehydrate_paket(self, booking):
        return str(booking.paket)
    def dehydrate_schlafen(self, booking):
        return str(booking.schlafen)
    def dehydrate_tshirt(self, booking):
        return str(booking.tshirt)
    def dehydrate_anreise(self, booking):
        return str(booking.anreise)
    def dehydrate_abreise(self, booking):
        return str(booking.abreise)
    def dehydrate_tarif(self, booking):
        return str(booking.tarif)
    def dehydrate_essen(self, booking):
        return booking.get_essen_display()
    def dehydrate_disziplinen(self, booking):
        return ", ".join(map(lambda x: x.code, booking.disziplinen.all()))


class BookingAdmin(ExportMixin, admin.ModelAdmin):
    list_display = ("event", "datum_short", "code", "nachname", "vorname", "geburtsdatum", "alter", "verein_ort", "paket", "schlafen", "essen", "show_paid", "show_offen", "colored_status", "checkin")
    list_filter = ("event", "checkin", "status", "paket", "schlafen", "essen", "verein_ort")
    search_fields = ("vorname", "nachname", "verein_ort", "code")
    list_display_links = ["code"]
    actions = [checkin]
    csv_fields = ("nachname", "vorname", "verein_ort", "paket", "code")
    resource_class = BookingResource

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if not request.user.is_superuser:
            if db_field.name == "event":
                kwargs["queryset"] = Event.objects.filter(admin=request.user)
            if db_field.name == "paket":
                kwargs["queryset"] = Package.objects.filter(event__admin=request.user)
            if db_field.name == "schlafen":
                kwargs["queryset"] = Sleep.objects.filter(packages__event__admin=request.user).distinct()


        return super(BookingAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

    def alter(self, obj):
        color = "green" if obj.volljaehrig() else "red"
        return format_html("<span style='color: {}'>{}</span>", color, obj.age().years)
    alter.admin_order_field = "geburtsdatum"
    alter.short_description = "Alter"

    def datum_short(self, obj):
        return format_html(defaultfilters.date(timezone.localtime(obj.datum), "SHORT_DATETIME_FORMAT"))
    datum_short.admin_order_field = 'datum'
    datum_short.short_description = 'Datum'

    fieldsets = (
        (None, {
            "fields": ["datum", "code", "betrag", "event"],
        }),
        ("Persönliche Daten", {
            "fields": [("vorname", "nachname", "geschlecht"), ("email", "verein_ort"), "geburtsdatum"],
        }),
        ("Anschrift & Telefon", {
            "fields": ["adresse", ("plz", "ort"), "land", "telefon"]
        }),
        ("Unterkunft, Verpflegung & sonstiges", {
            "fields": [("paket", "schlafen", "essen", "tshirt", "anreise", "abreise", "tarif"), ("disziplinen", )],
        }),
        ("Bearbeitung", {
            "fields": [("anmerkungen", "vermerk_intern"), "status"]
        }),
    )

    readonly_fields = ["betrag", "datum"]


    def get_queryset(self, request):
        qs = Booking.objects.prefetch_related("transaction_set").annotate(paid=Sum('transaction__betrag'), offen=F("betrag")-Sum("transaction__betrag"))
        if not request.user.is_superuser:
            return qs.filter(event__admin=request.user)
        return qs

    def colored_status(self, inst):
        color = "orange"
        if inst.status == "confirmed":
            color = "darkgreen"
        elif inst.status in ["problem", "canceled"]:
            color = "darkred"
        return format_html("<span style='color:{}'>{}</span>",color, inst.get_status_display())

    colored_status.short_description = "Status"
    colored_status.admin_order_field = "status"

    def show_paid(self, inst):
        return inst.paid or 0.0

    show_paid.admin_order_field = 'paid'
    show_paid.short_description = "bezahlt"

    def show_offen(self, inst):
        if inst.offen is None:
            return inst.betrag
        else:
            return inst.offen

    show_offen.admin_order_field = 'offen'
    show_offen.short_description = "offen"

    inlines = [ AnhangInline, TransactionInline ]

class TransactionResource(resources.ModelResource):
    class Meta:
        model = Transaction
    def dehydrate_booking(self, trans):
        return str(trans.booking)



class TransactionAdmin(ExportMixin, admin.ModelAdmin):
    list_display = ("id", "datum", "typ", "mittel", "nr", "betrag", "gebuehr", "booking")
    list_filter = ("booking__event",  )
    list_select_related = ("booking",)
    resource_class = TransactionResource

    def get_queryset(self, request):
        qs = Transaction.objects.all()
        if request.user.is_superuser:
            return qs
        return qs.filter(booking__event__admin=request.user)

class EventAdmin(admin.ModelAdmin):
    list_display = ("name", "ausrichter","von", "bis")
    prepopulated_fields = {"slug": ("name",)}

    fieldsets = (
        (None, {
            "fields": ["name", "slug", "beschreibung", "paketsystem_nutzen", "buchbar"]
        }),
        ("Zeit & Ort", {
            "fields": [("ausrichter", "von", "bis"),]
        }),
        ("Buchungsformular", {
            "fields": ["adresse", "telefon", "geschlecht"] 
        }),
        ("Verpflegung", {
            "fields": ["verpflegung", "vegetarisch", "vegan", "vegan_breakfast_only"]
        }),
        ("Zahlungsdaten", {
            "fields": ["paypal", ("kontoinhaber", "iban", "bic")]
        }),
        ("Ansprechpartner", {
            "fields": ["ansprechpartner", "kontakt"]
        }),
        ("Verwaltung", {
            "fields": ["admin"]
        })
    )

    inlines = [TarifInline, TagInline, TShirtInline, DokumentInline, DisziplinInline]

    def save_model(self, request, obj, form, change):             
        if not change:
            obj.admin = request.user
        obj.save()

    def get_readonly_fields(self, request, obj=None):
        if not request.user.is_superuser:
            return ["admin"]
        return []

    def get_queryset(self, request):
        qs = super(EventAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(admin=request.user)

class SeitenAdmin(admin.ModelAdmin):
    list_display = ("event", "slug", "name", "icon", "order")
    def get_queryset(self, request):
        qs = Seite.objects.all()
        if request.user.is_superuser:
            return qs
        return qs.filter(event__admin=request.user)

admin.site.register(Booking, BookingAdmin)
admin.site.register(Transaction, TransactionAdmin)
admin.site.register(Package, PackageAdmin)
admin.site.register(Sleep, SleepAdmin)
admin.site.register(Event, EventAdmin)
admin.site.register(Seite, SeitenAdmin)
