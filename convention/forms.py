from django import forms
from convention.models import Booking, Anhang, Dokument
import os.path

class BookingViewForm(forms.Form):
    code = forms.CharField(label="Buchungscode", max_length=8, required=True)
    email = forms.EmailField(label="E-Mail-Adresse", required=True)

    def clean(self):
        cd = super(BookingViewForm, self).clean()

        try:
            self.instance = Booking.objects.get(code=cd.get("code"), email=cd.get("email"))
        except Booking.DoesNotExist:
            raise forms.ValidationError("Buchung konnte nicht gefunden werden")
    
        return cd

class AnhangForm(forms.Form):
    email = forms.EmailField(required=True)
    code = forms.CharField(required=True)
    datei = forms.FileField(required=True)
    dokument = forms.ModelChoiceField(Dokument.objects.filter(upload=True), required=True)

    def clean(self):
        cd = super().clean()

        try:
            cd["booking"] = Booking.objects.get(code=cd.get("code"), email=cd.get("email"))
        except Booking.DoesNotExist:
            raise forms.ValidationError("Buchung konnte nicht gefunden werden")
    
        return cd

    def clean_datei(self):
        uploaded_file = self.cleaned_data['datei']
        try:
            # create an ImageField instance
            im = forms.ImageField()
            # now check if the file is a valid image
            im.to_python(uploaded_file)
        except forms.ValidationError:
            # file is not a valid image;
            # so check if it's a pdf
            name, ext = os.path.splitext(uploaded_file.name)
            if ext not in ['.pdf', '.PDF']:
                raise forms.ValidationError("Nur Bilder und PDF erlaubt")
        return uploaded_file

