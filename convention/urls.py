from django.conf.urls import url, include
from convention.views import reg, show_booking, participants_iframe, participants, checkin, seite, event_list, upload_attachment
from convention.api import router
from django.views.generic import TemplateView

app_name = "convention"
urlpatterns = [
    url(r'^api/', include(router.urls)),
    url(r'^$', event_list),
    url(r'^(?P<event_slug>.*)/reg/$', reg, name="reg"),
    url(r'^upload/$', upload_attachment, name="upload"),
    url(r'^(?P<event_slug>.*)/booking/$', show_booking, name="show-booking"),
    url(r'^(?P<event_slug>.*)/booking/(?P<booking_id>\d*)/checkin$', checkin, name="checkin"),
    url(r'^(?P<event_slug>.*)/booking/pdf/$', show_booking, { "pdf": True }, name="show-booking-pdf"),
    url(r'^(?P<event_slug>.*)/qr/$', show_booking, name="qr"),
    url(r'^(?P<event_slug>.*)/participants-iframe/$', participants_iframe, name="participants-iframe"),
    url(r'^(?P<event_slug>.*)/participants/$', participants, name="participants"),
    url(r'^(?P<event_slug>.*)/(?P<seite_slug>.*)/$', seite, name="seite"),
    url(r'^(?P<event_slug>.*)/$', seite, { "seite_slug": "home"}, name="home"),



]
