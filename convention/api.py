from django.contrib.auth.models import User
from convention.models import Package, Sleep, Booking, Disziplin, Event
from rest_framework import routers, serializers, viewsets, filters, mixins
from django.db.models import Sum, Count,  F, When, Case, IntegerField
from django.db.models.functions import Coalesce
from django_filters.rest_framework import DjangoFilterBackend


class EventSerializer(serializers.ModelSerializer):
    class Meta:
        model = Event
        fields = ("id", "slug", "name", "ausrichter", "beschreibung")

class PackageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Package
        fields = ("id", "code", "name", "kurz", "beschreibung", "preis", "naechte", "event")

class SleepSerializer(serializers.ModelSerializer):
    frei = serializers.IntegerField()
    class Meta:
        model = Sleep 
        fields = ("id", "code", "name", "beschreibung", "preis", "anzahl", "frei")

class BookingSerializer(serializers.ModelSerializer):
    disziplinen = serializers.PrimaryKeyRelatedField(many=True, queryset=Disziplin.objects.all(), required=False)

    class Meta:
        model = Booking
        fields = ("id", "disziplinen", "event", "code", "geburtsdatum", "email", "essen", "nachname", "paket", "schlafen", 
            "verein_ort", "vorname", "geschlecht", "anmerkungen", "adresse", "plz", "ort", "land", "telefon", "tshirt", "anreise", "abreise", "tarif")
        read_only_fields = ["id", "code"]

    def validate(self, attrs):
        # TODO: Serverseitige Validierung implementieren
        return super().validate(attrs)

# ViewSets define the view behavior.
class PackageViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = PackageSerializer
    queryset = Package.objects.filter(buchbar=True)
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ["event"]

class SleepViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = SleepSerializer

    filter_backends = (DjangoFilterBackend,)
    filter_fields = ["packages"]

    def get_queryset(self):
        return Sleep.objects.prefetch_related("packages", "booking_set").annotate(
            frei=F("anzahl")-Coalesce(Count(
                "booking"
            ), 0)
        )

class EventViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = EventSerializer
    queryset = Event.objects.all()

class BookingViewSet(viewsets.ModelViewSet):
    serializer_class = BookingSerializer
    queryset = Booking.objects.all()

    filter_backends = (DjangoFilterBackend, filters.OrderingFilter)
    filter_fields = ["event_id"]
    ordering_fields = ("datum", "vorname", "nachname")
    ordering = ("nachname", "vorname")



# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'packages', PackageViewSet)
router.register(r'sleep', SleepViewSet, base_name="sleep")
router.register(r'booking', BookingViewSet)
router.register(r'event', EventViewSet)
