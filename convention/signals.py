from django.dispatch import receiver
from django.db.models.signals import m2m_changed, post_save, pre_save, post_delete, pre_delete
from django.utils import timezone
import json
from .models import Booking, Transaction, Event
from django.core.mail import EmailMessage
from django.template.loader import render_to_string
from django.conf import settings
from paypal.standard.models import ST_PP_COMPLETED
from paypal.standard.ipn.signals import valid_ipn_received

# helper function
def send_mail(event, subject, body, from_email, to_emails, fail_silently=False):
    e = EmailMessage(
        subject,
        body,
        event.ansprechpartner + " <" + from_email + ">",
        to_emails
    )
    return e.send(fail_silently=fail_silently)

@receiver(valid_ipn_received)
def show_me_the_money(sender, **kwargs):
    ipn_obj = sender
    if ipn_obj.payment_status == ST_PP_COMPLETED:

        if ipn_obj.mc_currency == "EUR": # keine Fremdwaehrungen buchen
            code = ipn_obj.custom.lower()
            try:
                book = Booking.objects.get(code=code)
            except Booking.DoesNotExist:
                pass
            else:
                if book.event.paypal.lower() in [ipn_obj.receiver_email.lower(), ipn_obj.business.lower()]:
                    book.transaction_set.create(typ="incoming", mittel="paypal", betrag=ipn_obj.mc_gross, gebuehr=ipn_obj.mc_fee, nr=ipn_obj.txn_id, grund="Transaktion: " + ipn_obj.txn_id)
                    book.status = "progress"
                    book.save()

@receiver(post_save, sender=Event)
def create_homepage(sender, instance, created, **kwargs):
    if created:
        instance.seiten.create(name="Startseite", event=instance, slug="home", html="Die Startseite dieses Events sollte noch angepasst werden!")

@receiver(post_save, sender=Booking)
def notify_new(sender, instance, created, **kwargs):
    message = None
    if created:
        message = render_to_string("convention/mail_new.txt", { "booking": instance })
        send_mail(instance.event, "Buchung %s" % instance.code.upper(), message, settings.CONVENTION_EMAIL, [ instance.email ], fail_silently=True)

@receiver(post_save, sender=Transaction)
def notify_pay(sender, instance, created, **kwargs):
    message = None
    if created and instance.typ == "incoming":
        message = render_to_string("convention/mail_pay.txt", { "booking": instance.booking, "trans": instance })
        send_mail(instance.booking.event, "Zahlungseingang zu Buchung %s" % instance.booking.code.upper(), message, settings.CONVENTION_EMAIL, [ instance.booking.email ], fail_silently=True)
        


@receiver(pre_save, sender=Booking)
def calc_betrag(sender, instance, **kwargs):
    instance.betrag = instance.calc_betrag()

@receiver(pre_save, sender=Booking)
def notify_status(sender, instance, **kwargs):
    if instance.pk:
        old = Booking.objects.get(pk=instance.pk)
        if old.status != instance.status:
            message = render_to_string("convention/mail_status.txt", { "booking": instance })
            send_mail(instance.event, "Update zu Buchung %s" % instance.code.upper(), message, settings.CONVENTION_EMAIL, [ instance.email ], fail_silently=True)

