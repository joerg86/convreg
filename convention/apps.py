from django.apps import AppConfig

class ConventionConfig(AppConfig):
    name = "convention"
    verbose_name = "Convention-Registrierung"

    def ready(self):
        from . import signals
