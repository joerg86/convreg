# +-+ coding: utf-8 +-+

from __future__ import unicode_literals

from django.db import models
import random
import string
from django.utils import timezone
from django.urls import reverse
from django.utils.http import urlencode
from localflavor.generic.models import IBANField, BICField

# Create your models here.

SCHLAFEN_CHOICES = (
    ("none", "Keiner"),
    ("floor", "Auf dem Boden"),
    ("dorm", "Bett im Schlafsaal"),
    ("four", "Bett im Viererzimmer"),
    ("dbl", "Bett im Doppelzimmer "),
    ("dbl_ensuite", "Bett im Doppelzimmer Du/WC"),
)

ESSEN_CHOICES = (
    ("all", "alles"),
    ("v", "vegetarisch"),
    ("vv", "vegan"),
)

PAKET_CHOICES = (
    ("all", u"All inclusive"),
    ("late", u"All inclusive - Spätanreise"),
    ("day", u"Tagesgast"),
)

STATUS_CHOICES = (
    ("open", "Offen"),
    ("progress", "In Bearbeitung"),
    ("confirmed", u"Bestätigt"),
    ("problem", u"Problem - Teilnehmer kontaktiert"),
    ("canceled", u"Storno"),
)

class Event(models.Model):
    name = models.CharField(max_length=100)
    slug = models.SlugField("Kürzel", help_text="Kürzel für die URL, nur Buchstaben, Zahlen und Striche, keine Leerzeichen, z.B. 'odm-steinach-2017'", unique=True)
    von = models.DateTimeField()
    bis = models.DateTimeField()
    beschreibung = models.TextField()

    kontakt = models.EmailField("E-Mail-Kontakt")
    ansprechpartner = models.CharField("Name", max_length=100)
    ausrichter = models.CharField(help_text="Name des Ausrichters, z.B. 'SV Pusemuckel'", max_length=100)

    paypal = models.EmailField("PayPal-Adresse", help_text="An diese Adresse werde Zahlungen gesendet", blank=True)
    kontoinhaber = models.CharField(max_length=100, blank=True)
    bic = BICField("BIC", blank=True)
    iban = IBANField("IBAN", blank=True)

    admin = models.ForeignKey("auth.User", help_text="Dieser Benutzer kann das Event verwalten")

    def get_absolute_url(self):
        return reverse("convention:seite", args=[self.slug])

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = "Event"
        verbose_name_plural = "Events"
        ordering = ("von", "name")

class Seite(models.Model):
    event = models.ForeignKey("Event", related_name="seiten")
    slug = models.SlugField(default="home")
    name = models.CharField(max_length=30)
    icon = models.CharField(max_length=30, default="home")
    html = models.TextField(blank=True, null=True)
    order = models.PositiveIntegerField(default=0)

    class Meta:
        verbose_name = "Seiten"
        verbose_name_plural = "Seiten"
        unique_together = ("event", "slug")
        ordering = ("order",)


class Trainer(models.Model):
    event = models.ForeignKey("Event")
    vorname = models.CharField(max_length=100)
    nachname = models.CharField(max_length=100)
    foto = models.ImageField(blank=True, null=True)
    beschreibung = models.TextField()
    order = models.PositiveIntegerField(default=0)

    class Meta:
        ordering = ("order", "nachname", "vorname")
        verbose_name = "Trainer"
        verbose_name_plural = "Trainer"

    def __unicode__(self):
        return "%s %s" % (self.vorname, self.nachname)


def generate_code():
    return "".join(random.choice(string.ascii_lowercase+string.digits) for i in range(8))

class Booking(models.Model):
    event = models.ForeignKey("Event")
    code = models.CharField(max_length=8, unique=True, default=generate_code)
    datum = models.DateTimeField(auto_now_add=True)
    checkin = models.DateTimeField(null=True, blank=True, editable=False)

    vorname = models.CharField("Vorname", max_length=100)
    nachname = models.CharField("Nachname", max_length=100)
    email = models.EmailField("E-Mail")
    verein_ort = models.CharField("Verein oder Ort", max_length=100, blank=True)
    geburtsdatum = models.DateField()

    paket = models.ForeignKey("Package", verbose_name="Paket")
    schlafen = models.ForeignKey("Sleep", verbose_name="Schlafplatz", null=True, blank=True)

    essen = models.CharField("Verpflegung", max_length=15, choices=ESSEN_CHOICES)

    anmerkungen = models.TextField("Anmerkungen", blank=True)

    betrag = models.DecimalField("Betrag", editable=False, max_digits=8, decimal_places=2, default=0, help_text="Gesamter vom Teilnehmer zu zahlender Betrag. Wird automatisch berechnet.")
    status = models.CharField("Status", max_length=15, choices=STATUS_CHOICES, default="open")

    vermerk_intern = models.TextField(u"Interne Vermerke", blank=True)

    def get_absolute_url(self):
        return reverse("convention:show-booking", args=[self.event.slug]) + "?" + urlencode({ "code": self.code, "email": self.email })

    def calc_betrag(self):
        betrag = 0
       
        betrag += self.paket.preis

        if self.schlafen:
            betrag += self.paket.naechte * self.schlafen.preis
 
        return betrag



    class Meta:
        verbose_name = u"Buchung"
        verbose_name_plural = u"Buchungen"

        ordering = ("-datum", "nachname", "vorname", "code")

    def __unicode__(self):
        return self.code

class Package(models.Model):
    event = models.ForeignKey("Event")
    code = models.CharField("Buchungscode", unique=True, max_length=15)
    name = models.CharField("Name", max_length=100)
    beschreibung = models.TextField("Bescheribung", help_text="HTML erlaubt")
    kurz = models.TextField("Kurzbeschreibung")
    preis = models.DecimalField("Preis", max_digits=8, decimal_places=2)
    naechte = models.PositiveIntegerField(u"Anzahl Nächte", default=2)
    order = models.PositiveIntegerField("Sortierung", default=0)
    buchbar = models.BooleanField(default=True, help_text="Paket derzeit buchbar")

    def __unicode__(self):
        return self.code

    class Meta:
        verbose_name = "Paket"
        verbose_name_plural = "Pakete"
        ordering = ("order", "name")
    
    
class Sleep(models.Model):
    code = models.CharField("Buchungscode", unique=True, max_length=15)
    name = models.CharField("Name", max_length=100)
    beschreibung = models.TextField("Bescheribung", help_text="HTML erlaubt")
    preis = models.DecimalField("Preis", max_digits=8, decimal_places=2, help_text="Preis pro Nacht")
    anzahl = models.PositiveIntegerField("Anzahl", help_text=u"Anzahl der insgesamt verfügbaren Betten")
    bettwaesche = models.BooleanField(default=True, help_text=u"Bettwäsche ist verpflichtend")
    order = models.PositiveIntegerField("Sortierung", default=0)

    packages = models.ManyToManyField("Package", verbose_name="Pakete", help_text=u"Pakaete, in denen der Schlafplatz verfügbar ist")

    def __unicode__(self):
        return self.code

    class Meta:
        verbose_name = "Schlafplatz"
        verbose_name_plural = u"Schlafplätze"
        ordering = ("order", "name")
    
    
    
    

TRANS_TYP_CHOICES = (
    ("incoming", "Zahlungseingang"),
    ("credit", "Gutschrift"),
    ("refund", "Erstattung"),
    ("other", "Anderer"),
)

TRANS_MITTEL_CHOICES = (
    ("paypal", "PayPal"),
    ("cash", "bar"),
    ("wire", u"Überweisung"),
    ("internal", u"Interne Buchung"),
)

class Transaction(models.Model):
    booking = models.ForeignKey("Booking", verbose_name="Buchung")
    typ = models.CharField("Typ", max_length=15, choices=TRANS_TYP_CHOICES)
    mittel = models.CharField("Zahlungsmittel", max_length=15, choices=TRANS_MITTEL_CHOICES)
    nr = models.CharField("Nr.", max_length=255, blank=True, default="", help_text="Nr. der Transaktion beim Zahlungsdienstleister, falls vorhanden")
    betrag = models.DecimalField("Betrag", max_digits=8, decimal_places=2, default=0, help_text=u"Bei ausgehenden Zahlungen bitte negatives Vorzeichen benutzen")
    grund = models.CharField(max_length=255, blank=True, default="")
    datum = models.DateTimeField(default=timezone.now)
    

    class Meta:
        verbose_name = u"Transaktion"
        verbose_name_plural = u"Transaktionen"

        ordering = ("-datum",)

    def __unicode__(self):
        return str(self.id)
