# +-+ coding: utf-8 +-+

from django.shortcuts import render, redirect, get_object_or_404
from django.template.loader import render_to_string
from convention.models import Package, Booking, Transaction, Event, Seite, Anhang
from convention.forms import BookingViewForm, AnhangForm
from django.db.models import F, Sum
from django.urls import reverse
from paypal.standard.forms import PayPalPaymentsForm
from django.views.decorators.csrf import csrf_exempt
from django.utils.http import urlencode, urlquote
from django.utils import timezone
#from django_xhtml2pdf.utils import generate_pdf
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from weasyprint import HTML
from django.core.exceptions import PermissionDenied

# Create your views here.

def event_list(request):
    context = {
        "event_list": Event.objects.filter(von__gt=timezone.now()),
    }
    return render(request, "convention/event_list.html", context)

def seite(request, event_slug, seite_slug):
    seite = get_object_or_404(Seite, slug=seite_slug, event__slug=event_slug)
    context = {
        "seite": seite,
        "active_page": seite.slug,
        "event": seite.event,
        "seiten": seite.event.seiten.filter(menu=True),

    }
    return render(request, "convention/seite.html", context)

def participants_iframe(request, event_slug):
    event = get_object_or_404(Event, slug=event_slug)

    context = {
        "bookings": Booking.objects.filter(event=event).order_by("datum"),
        "event": event,
        "seiten": event.seiten.filter(menu=True),

    }
    return render(request, "convention/participants_iframe.html", context)

# iframe with participants list for the main site
def participants(request, event_slug):
    event = get_object_or_404(Event, slug=event_slug)

    context = {
        "bookings": Booking.objects.filter(event=event).order_by("nachname", "vorname"),
        "event": event,
        "seiten": event.seiten.filter(menu=True),

        "active": "participants",
    }
    return render(request, "convention/participants.html", context)


@login_required
def checkin(request, event_slug, booking_id):
    event = get_object_or_404(Event, slug=event_slug)

    booking = get_object_or_404(Booking, pk=booking_id)
    booking.checkin = timezone.now()
    booking.save()
    return redirect(booking)
        

# registration
def reg(request, event_slug):
    event = get_object_or_404(Event, slug=event_slug)
    if not event.buchbar:
        raise PermissionDenied()

    context = {
        "active": "reg",
        "event": event,
        "seiten": event.seiten.filter(menu=True),

    }

    return render(request, "convention/reg.html", context)

def upload_attachment(request):
    form = AnhangForm(request.POST, request.FILES)
    if form.is_valid():
        cd = form.cleaned_data
        booking = cd["booking"]
        dokument = cd["dokument"]
        datei = cd["datei"]
        anhang, created = Anhang.objects.get_or_create(booking=booking, dokument=dokument, defaults={"datei": datei})
        if not created:
            anhang.datei = datei
            anhang.save()

        return redirect(booking)
    else:
        raise PermissionDenied()

# show booking and PP return / cancel page
@csrf_exempt
def show_booking(request, event_slug, pdf=False):
    event = get_object_or_404(Event, slug=event_slug)

    booking = None

    if "code" in request.GET:
        form = BookingViewForm(request.GET)
        if form.is_valid():
            booking = form.instance
    else:
        form = BookingViewForm()

    bezahlt = 0
    offen = 0
    
    if booking:
        offen = booking.betrag
        aggr = booking.transaction_set.all().aggregate(bezahlt=Sum("betrag"))
        bezahlt = aggr["bezahlt"] or 0
        offen = booking.betrag - bezahlt
   
    transactions = None
    dokumente = None
    dokumente_fehlend = None
    qr_url = None
    print_url = None
    if booking:
        transactions = booking.transaction_set.all().order_by("datum")
        
        if booking.volljaehrig():
            dokumente = booking.event.dokumente.exclude(u18=True)
        else:
            dokumente = booking.event.dokumente.all()
        dokumente_fehlend = dokumente.filter(upload=True).exclude(anhang__booking=booking)

        for dok in dokumente:
            dok.anhang = dok.anhang_set.filter(booking=booking).first()

        qr_url = urlquote(request.build_absolute_uri(reverse("convention:qr", args=[event.slug])+"?"+urlencode({ "code": booking.code, "email": booking.email })))
        print_url = reverse("convention:show-booking-pdf", args=[event.slug])+"?"+urlencode({ "code": booking.code, "email": booking.email })

    # PayPal
    # What you want the button to do.
    paypal_form = None

    if booking and offen > 0:
        paypal_dict = {
            "business": event.paypal,
            "amount": offen,
            "currency_code": "EUR",
            "lc": "DE",
            "item_name": "%s %s" % (event.name, booking.code.upper()),
            "custom": booking.code.upper(),
            "notify_url": request.build_absolute_uri(reverse('paypal-ipn')),
            "return_url": request.build_absolute_uri(booking.get_absolute_url()),
            "cancel_return": request.build_absolute_uri(booking.get_absolute_url()),
        }

        # Create the instance.
        paypal_form = PayPalPaymentsForm(initial=paypal_dict)

    context = {
        "event": event,
        "seiten": event.seiten.filter(menu=True),
        "form": form,
        "booking": booking,
        "bezahlt": bezahlt,
        "offen": offen,
        "transactions": transactions,
        "dokumente": dokumente,
        "dokumente_fehlend": dokumente_fehlend,
        "qr_url": qr_url,
        "print_url": print_url,
        "active": "show-booking",
        "paypal_form": paypal_form,
    }

    if pdf:
        html = render_to_string("convention/booking_pdf.html", context=context, request=request)
        pdf = HTML(string=html).write_pdf()
        resp = HttpResponse(pdf, content_type='application/pdf')

        return resp
    else:
        return render(request, "convention/booking.html", context)
