# +-+ coding: utf-8 +-+



from django.db import models
import random
import string
from django.utils import timezone
from django.urls import reverse
from django.utils.http import urlencode
from localflavor.generic.models import IBANField, BICField
from ckeditor_uploader.fields import RichTextUploadingField
from django_countries.fields import CountryField
from datetime import date
from dateutil.relativedelta import relativedelta

# Create your models here.

SCHLAFEN_CHOICES = (
    ("none", "Keiner"),
    ("floor", "Auf dem Boden"),
    ("dorm", "Bett im Schlafsaal"),
    ("four", "Bett im Viererzimmer"),
    ("dbl", "Bett im Doppelzimmer "),
    ("dbl_ensuite", "Bett im Doppelzimmer Du/WC"),
)

ESSEN_CHOICES = (
    ("all", "alles"),
    ("v", "vegetarisch"),
    ("vv", "vegan"),
)

PAKET_CHOICES = (
    ("all", "All inclusive"),
    ("late", "All inclusive - Spätanreise"),
    ("day", "Tagesgast"),
)

STATUS_CHOICES = (
    ("open", "Offen"),
    ("progress", "In Bearbeitung"),
    ("confirmed", "Bestätigt"),
    ("problem", "Problem - Teilnehmer kontaktiert"),
    ("canceled", "Storno"),
)

GESCHLECHT_CHOICES = (
    ("f", "weiblich"),
    ("m", "männlich"),
    ("d", "divers"),
)

class Event(models.Model):
    name = models.CharField(max_length=100)
    slug = models.SlugField("Kürzel", help_text="Kürzel für die URL, nur Buchstaben, Zahlen und Striche, keine Leerzeichen, z.B. 'odm-steinach-2017'", unique=True)
    von = models.DateTimeField()
    bis = models.DateTimeField()
    beschreibung = models.TextField()
    buchbar = models.BooleanField(default=True, help_text="Event derzeit buchbar")

    kontakt = models.EmailField("E-Mail-Kontakt")
    ansprechpartner = models.CharField("Name", max_length=100)
    ausrichter = models.CharField(help_text="Name des Ausrichters, z.B. 'SV Pusemuckel'", max_length=100)

    paypal = models.EmailField("PayPal-Adresse", help_text="An diese Adresse werde Zahlungen gesendet", blank=True)
    kontoinhaber = models.CharField(max_length=100, blank=True)
    bic = BICField("BIC", blank=True)
    iban = IBANField("IBAN", blank=True)

    adresse = models.BooleanField("Adresse erforderlich", help_text="Ist bei der Anmeldung die Eingabe der Postadresse erforderlich?", default=False)
    telefon = models.BooleanField("Telefon erforderlich", help_text="Ist bei der Anmeldung die Eingabe einer (Notfall-)Telefonnummer erforderlich?", default=False)
    geschlecht  = models.BooleanField("Geschlecht erforderlich", help_text="Ist bei der Anmeldung die Eingabe des Geschlechts erforderlich?", default=False)

    verpflegung = models.BooleanField("Verpflegung", help_text="Es wird Verpflegung angeboten", default=True)
    vegetarisch = models.BooleanField("Vegetarisch", help_text="Es kann vegetarisches Essen gewählt werden.", default=True)
    vegan = models.BooleanField("Vegan", help_text="Es kann veganes Essen gewählt werden.", default=True)
    vegan_breakfast_only = models.BooleanField("Nur veganes Frühstück", help_text="Der Benutzer wird bei der Buchung darauf hingewiesen, dass nur das Frühstück vegan ist.", default=False)

    paketsystem_nutzen = models.BooleanField("Paketsystem nutzen", help_text="Teilnehmer können verschiedene Pakete benutzen (altes System).", default=True)

    admin = models.ForeignKey("auth.User", on_delete=models.CASCADE, help_text="Dieser Benutzer kann das Event verwalten")

    def get_absolute_url(self):
        return reverse("convention:seite", args=[self.slug])

    def __str__(self):
        return self.name

    @property
    def anreise(self):
        return self.tage.filter(anreise=True)

    @property
    def abreise(self):
        return self.tage.filter(abreise=True)

    @property
    def tarife_buchbar(self):
        return self.tarife.filter(buchbar=True)


    class Meta:
        verbose_name = "Event"
        verbose_name_plural = "Events"
        ordering = ("von", "name")  

class Tarif(models.Model):
    event = models.ForeignKey("Event", related_name="tarife", on_delete=models.CASCADE)
    tarif = models.CharField(max_length=100)
    preis = models.DecimalField("Preis pro Nacht", max_digits=8, decimal_places=2, null=True, blank=True)
    kostenstopp = models.DecimalField("Kostenstopp", max_digits=8, decimal_places=2, null=True, blank=True)
    buchbar = models.BooleanField("Aktuell buchbar", default=True)

    order = models.PositiveIntegerField("Sortierung", default=0)

    def __str__(self):
        return self.tarif

    class Meta:
        ordering = ("order", "tarif")

class Dokument(models.Model):
    event = models.ForeignKey("Event", related_name="dokumente", on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    dokument = models.FileField("Dokument", upload_to="dokumente")
    u18 = models.BooleanField("U18", default=False, help_text="Nur relevant für Personen unter 18 Jahren.")
    upload = models.BooleanField("Upload für Rücksendung", default=False, help_text="Dokument muss zurückgesendet werden über die Upload-Funktion.")
    order = models.PositiveIntegerField("Sortierung", default=0)


    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Dokument"
        verbose_name_plural = "Dokumente"
        ordering = ("order", "name")

def anhang_path(instance, filename):
    return "attachments/%s/%s" % (instance.booking.code, filename)

class Anhang(models.Model):
    booking = models.ForeignKey("Booking", on_delete=models.CASCADE)
    dokument = models.ForeignKey("Dokument", on_delete=models.CASCADE)
    datei = models.FileField(upload_to=anhang_path)
    datum = models.DateTimeField(auto_now=True)

    class Meta:
        unique_together = ("booking", "dokument")
        verbose_name = "Anhang"
        verbose_name_plural = "Anhänge"

class TShirt(models.Model):
    event = models.ForeignKey("event", on_delete=models.CASCADE, related_name="tshirts")
    groesse = models.CharField("Größe", max_length=100)
    preis = models.DecimalField("Aufpreis", max_digits=8, decimal_places=2, default=0)
    order = models.PositiveIntegerField("Sortierung", default=0)

    def __str__(self):
        return self.groesse

    class Meta:
        verbose_name = "T-Shirt"
        verbose_name_plural = "T-Shirts"
        ordering = ("order", "groesse")

class Tag(models.Model):
    event = models.ForeignKey("event", on_delete=models.CASCADE, related_name="tage")
    tag = models.CharField("Tag", max_length=100)

    anreise = models.BooleanField("Anreise möglich", default=False)
    abreise = models.BooleanField("Abreise möglich", default=False)
    
    order = models.PositiveIntegerField("Sortierung", default=0)

    def __str__(self):
        return self.tag

    class Meta:
        verbose_name = "Tag"
        verbose_name_plural = "Tage"
        ordering = ("order", "tag")

class Disziplin(models.Model):
    event = models.ForeignKey("event", on_delete=models.CASCADE, related_name="disziplinen")
    code = models.CharField(max_length=10)
    label = models.CharField(max_length=100)
    order = models.PositiveIntegerField("Sortierung", default=0)

    def __str__(self):
        return self.label
    
    class Meta:
        verbose_name = "Disziplin"
        verbose_name_plural = "Disziplinen"
        ordering = ("order", "code")

class Seite(models.Model):
    event = models.ForeignKey("Event", on_delete=models.CASCADE, related_name="seiten")
    slug = models.SlugField(default="home")
    name = models.CharField(max_length=30)
    icon = models.CharField(max_length=30, default="home")
    html = RichTextUploadingField(blank=True, null=True)
    order = models.PositiveIntegerField(default=0)
    menu = models.BooleanField("Sichtbar im Menü", default=True)

    class Meta:
        verbose_name = "Seiten"
        verbose_name_plural = "Seiten"
        unique_together = ("event", "slug")
        ordering = ("order",)


def generate_code():
    return "".join(random.choice(string.ascii_lowercase+string.digits) for i in range(8))

class Booking(models.Model):
    event = models.ForeignKey("Event", on_delete=models.CASCADE)
    code = models.CharField(max_length=8, unique=True, default=generate_code)
    datum = models.DateTimeField(auto_now_add=True)
    checkin = models.DateTimeField(null=True, blank=True, editable=False)

    vorname = models.CharField("Vorname", max_length=100)
    nachname = models.CharField("Nachname", max_length=100)
    geschlecht = models.CharField("Geschlecht", max_length=1, choices=GESCHLECHT_CHOICES, null=True, blank=True)

    email = models.EmailField("E-Mail")
    verein_ort = models.CharField("Verein oder Ort", max_length=100, blank=True)
    geburtsdatum = models.DateField()

    adresse = models.CharField(max_length=255, blank=True, null=True)
    plz = models.CharField("PLZ", max_length=20, blank=True, null=True)
    ort = models.CharField(max_length=100, blank=True, null=True)
    land = CountryField(default="DE", blank=True, null=True)
    telefon = models.CharField(max_length=20, blank=True, null=True)

    paket = models.ForeignKey("Package", null=True, blank=True, on_delete=models.SET_NULL, verbose_name="Paket")
    schlafen = models.ForeignKey("Sleep", on_delete=models.SET_NULL, verbose_name="Schlafplatz", null=True, blank=True)

    essen = models.CharField("Verpflegung", max_length=15, choices=ESSEN_CHOICES)
    tshirt = models.ForeignKey("TShirt", null=True, blank=True, on_delete=models.SET_NULL, verbose_name="T-Shirt")
    anreise = models.ForeignKey("Tag", null=True, blank=True, on_delete=models.SET_NULL, verbose_name="Anreise", related_name="booking_anreise")
    abreise = models.ForeignKey("Tag", null=True, blank=True, on_delete=models.SET_NULL, verbose_name="Abreise", related_name="booking_abreise")
    tarif = models.ForeignKey("Tarif", null=True, blank=True, on_delete=models.SET_NULL)
    disziplinen = models.ManyToManyField("convention.Disziplin", null=True, blank=True)



    anmerkungen = models.TextField("Anmerkungen", blank=True)

    betrag = models.DecimalField("Betrag", editable=False, max_digits=8, decimal_places=2, default=0, help_text="Gesamter vom Teilnehmer zu zahlender Betrag. Wird automatisch berechnet.")
    status = models.CharField("Status", max_length=15, choices=STATUS_CHOICES, default="open")

    vermerk_intern = models.TextField("Interne Vermerke", blank=True)

    def get_absolute_url(self):
        return reverse("convention:show-booking", args=[self.event.slug]) + "?" + urlencode({ "code": self.code, "email": self.email })

    def calc_betrag(self):
        betrag = 0

        if self.event.paketsystem_nutzen:
            if self.paket:
                betrag += self.paket.preis

                if self.schlafen:
                    betrag += self.paket.naechte * self.schlafen.preis
        else:
            if self.tarif and self.anreise and self.abreise:
                betrag = min(self.tarif.preis*(self.abreise.order - self.anreise.order), self.tarif.kostenstopp)
 
        if self.tshirt.preis:
            betrag += self.tshirt.preis

        return betrag


    def age(self):
        age = relativedelta(self.event.von.date(), self.geburtsdatum)
        return age

    def volljaehrig(self):
        age = self.age()
        if age.years >= 18:
            return True
        else:
            return False



    class Meta:
        verbose_name = "Buchung"
        verbose_name_plural = "Buchungen"

        ordering = ("-datum",)

    def __str__(self):
        return self.code

class Package(models.Model):
    event = models.ForeignKey("Event", on_delete=models.CASCADE)
    code = models.CharField("Buchungscode", unique=True, max_length=15)
    name = models.CharField("Name", max_length=100)
    beschreibung = models.TextField("Bescheribung", help_text="HTML erlaubt")
    kurz = models.TextField("Kurzbeschreibung")
    preis = models.DecimalField("Preis", max_digits=8, decimal_places=2)
    naechte = models.PositiveIntegerField("Anzahl Nächte", default=2)
    order = models.PositiveIntegerField("Sortierung", default=0)
    buchbar = models.BooleanField(default=True, help_text="Paket derzeit buchbar")

    def __str__(self):
        return self.code

    class Meta:
        verbose_name = "Paket"
        verbose_name_plural = "Pakete"
        ordering = ("order", "name")
    
    
class Sleep(models.Model):
    code = models.CharField("Buchungscode", unique=True, max_length=15)
    name = models.CharField("Name", max_length=100)
    beschreibung = models.TextField("Bescheribung", help_text="HTML erlaubt")
    preis = models.DecimalField("Preis", max_digits=8, decimal_places=2, help_text="Preis pro Nacht")
    anzahl = models.PositiveIntegerField("Anzahl", help_text="Anzahl der insgesamt verfügbaren Betten")
    bettwaesche = models.BooleanField(default=True, help_text="Bettwäsche ist verpflichtend")
    order = models.PositiveIntegerField("Sortierung", default=0)

    packages = models.ManyToManyField("Package", verbose_name="Pakete", help_text="Pakaete, in denen der Schlafplatz verfügbar ist")

    def __str__(self):
        return self.code

    class Meta:
        verbose_name = "Schlafplatz"
        verbose_name_plural = "Schlafplätze"
        ordering = ("order", "name")
    
    
    
    

TRANS_TYP_CHOICES = (
    ("incoming", "Zahlungseingang"),
    ("credit", "Gutschrift"),
    ("refund", "Erstattung"),
    ("other", "Anderer"),
)

TRANS_MITTEL_CHOICES = (
    ("paypal", "PayPal"),
    ("cash", "bar"),
    ("wire", "Überweisung"),
    ("internal", "Interne Buchung"),
)

class Transaction(models.Model):
    booking = models.ForeignKey("Booking", on_delete=models.CASCADE, verbose_name="Buchung")
    typ = models.CharField("Typ", max_length=15, choices=TRANS_TYP_CHOICES)
    mittel = models.CharField("Zahlungsmittel", max_length=15, choices=TRANS_MITTEL_CHOICES)
    nr = models.CharField("Nr.", max_length=255, blank=True, default="", help_text="Nr. der Transaktion beim Zahlungsdienstleister, falls vorhanden")
    betrag = models.DecimalField("Betrag", max_digits=8, decimal_places=2, default=0, help_text="Bei ausgehenden Zahlungen bitte negatives Vorzeichen benutzen")
    gebuehr = models.DecimalField("Gebühr", max_digits=8, decimal_places=2, default=0, help_text="Transaktionsgebühr (z.B. bei PayPal)")
    grund = models.CharField(max_length=255, blank=True, default="")
    datum = models.DateTimeField(default=timezone.now)
    

    class Meta:
        verbose_name = "Transaktion"
        verbose_name_plural = "Transaktionen"

        ordering = ("-datum",)

    def __str__(self):
        return str(self.id)
