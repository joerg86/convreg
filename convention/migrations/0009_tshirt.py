# Generated by Django 2.0.5 on 2018-06-06 16:15

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('convention', '0008_auto_20180606_1401'),
    ]

    operations = [
        migrations.CreateModel(
            name='TShirt',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('groesse', models.CharField(max_length=100)),
                ('event', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='convention.Event')),
            ],
            options={
                'verbose_name_plural': 'T-Shirts',
                'verbose_name': 'T-Shirt',
                'ordering': ('groesse',),
            },
        ),
    ]
