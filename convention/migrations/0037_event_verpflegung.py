# Generated by Django 2.0.5 on 2019-08-10 15:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('convention', '0036_booking_disziplinen'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='verpflegung',
            field=models.BooleanField(default=True, help_text='Es wird Verpflegung angeboten', verbose_name='Verpflegung'),
        ),
    ]
