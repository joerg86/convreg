# Generated by Django 2.0.5 on 2019-02-22 09:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('convention', '0024_auto_20190222_0949'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='paketsystem_nutzen',
            field=models.BooleanField(default=True, help_text='Teilnehmer können verschiedene Pakete benutzen (altes System).', verbose_name='Paketsystem nutzen'),
        ),
        migrations.AddField(
            model_name='event',
            name='preis',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=8, null=True, verbose_name='Preis pro Nacht'),
        ),
    ]
