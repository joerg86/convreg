# Generated by Django 2.0.5 on 2019-06-25 08:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('convention', '0031_auto_20190424_2305'),
    ]

    operations = [
        migrations.AddField(
            model_name='tarif',
            name='buchbar',
            field=models.BooleanField(default=True, verbose_name='Aktuell buchbar'),
        ),
    ]
