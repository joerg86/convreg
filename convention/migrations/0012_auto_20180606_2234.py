# Generated by Django 2.0.5 on 2018-06-06 20:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('convention', '0011_booking_tshirt'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='booking',
            options={'ordering': ('-datum',), 'verbose_name': 'Buchung', 'verbose_name_plural': 'Buchungen'},
        ),
        migrations.AddField(
            model_name='seite',
            name='menu',
            field=models.BooleanField(default=True, verbose_name='Sichtbar im Menü'),
        ),
    ]
