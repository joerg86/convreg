# Generated by Django 2.0.5 on 2019-08-16 15:05

from django.db import migrations

def migrate_txns(apps, schema_editor):
    PayPalIPN = apps.get_model("ipn", "PayPalIPN")
    Transaction = apps.get_model("convention", "Transaction")
    for ipn in PayPalIPN.objects.all():
        Transaction.objects.filter(nr=ipn.txn_id).update(gebuehr=ipn.mc_fee)


class Migration(migrations.Migration):

    dependencies = [
        ('convention', '0039_transaction_gebuehr'),
        ('ipn', '0008_auto_20181128_1032')
    ]

    operations = [
        migrations.RunPython(migrate_txns),
    ]
